#!/usr/bin/python
# -- coding : utf-8 --

###
###  This script is only for academic use
###  Do not use this script and all its related results for business purpose
###  The author has no relationship with people who abuse this program
###
###  @author zhenyi2697 at gmail.com

import os
import sys
import urllib
import urllib2
import argparse
import time
import PIL
import Image
import _imaging
from random import randint
import requests
from StringIO import StringIO

headers = {
        'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'en-US,en;q=0.5',
        'Connection': 'keep-alive',
        'Host': 'wxs.ign.fr',
        'Referer': 'http://www.geoportail.gouv.fr/swf/geoportal-visu-1.3.2.swf',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0',
}

def get_image(url, image_name):

    # urllib.urlretrieve(url, image_name)

    req = urllib2.Request(url, None, headers)
    r = urllib2.urlopen(req)
    with open(image_name, 'w+') as f:
        f.write(r.read())

    ### First try with image downloaded with urllib2
    try:
        im = Image.open(image_name)
    except IOError, e:

        sys.stdout.write('IOError, reloading image %s \nWaiting ... \n' % image_name)
        sys.stdout.flush()
        time.sleep(10)

        ### Second try, with image downloaded with urllib
        urllib.urlretrieve(url, image_name)

        try:
            im = Image.open(image_name)
        except IOError, e2:

            sys.stdout.write('Reloading failed ... now change another way to download image ...\nWaiting ...\n')
            sys.stdout.flush()
            time.sleep(10)

            ### Third try, use requests to download image
            try:
                r = requests.get(url, headers=headers)
                im = Image.open(StringIO(r.content))

            except IOError, e3:

                sys.stdout.write('Give up ... create a empty image instead\n')
                sys.stdout.write('Failed to download image %s\n' % image_name)
                sys.stdout.flush()
                empty_img = Image.new('RGB', (256, 256))
                empty_img.save(image_name)
                im = Image.open(image_name)
    return im


def get_map(args):

    ### get args
    row_start = int(args.get('row', 11320))
    col_start = int(args.get('column', 16620))
    row_span = col_span = int(args.get('span', 5))
    matrix = args.get('matrix', 15) or 15
    output = args.get('output', 'my_map.jpg') or 'my_map.jpg'
    bruteforce = args.get('bruteforce', False) or False 

    url_prefix = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&'
    url_appendix = '&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='

    ### Create an empty map
    my_map = Image.new('RGB', (256 * col_span, 256 * row_span))

    for row in xrange(row_span):

        sys.stdout.write('Saving row: %d ' % (row_start + row) )
        sys.stdout.flush()

        for col in xrange(col_span):

            image_name = str(row_start + row) + '-' + str(col_start + col) + '.jpg'

            ### First, download image from url
            url = url_prefix + 'TILEMATRIX=' + str(matrix) +'&TILEROW=' + str(row_start + row) + '&TILECOL=' + str(col_start + col) + url_appendix
            im = get_image(url, image_name)

            ### Then, paste image to map
            my_map.paste(im, (col * 256, row * 256))

            ### Last, remove tile image
            try:
                os.remove(image_name)
            except:
                pass
            sys.stdout.write('.')
            sys.stdout.flush()
            if not bruteforce:
                time.sleep(0.3)

        sys.stdout.write('\nCalm down, don\'t be too violent ... \n')
        sys.stdout.flush()
        if not bruteforce:
            time.sleep(randint(3, 5))

    sys.stdout.write('Combining image %s ...\n' % output)
    my_map.save(output)
    sys.stdout.write('Image %s saved successfully !\n' % output)
    sys.stdout.flush()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Script for getting map from geoportail.fr')
    parser.add_argument('--row', action='store', help='Row of start point', metavar='<ROW_NUM>', required=True)
    parser.add_argument('--column', action='store', help='Column of start point', metavar='<COL_NUM>', required=True)
    parser.add_argument('--span', action='store', help='Size of map (number of tile map)', metavar='<SPAN>', required=True)
    parser.add_argument('--matrix', action='store', help='The tile matrix number', metavar='<MATRIX>')
    parser.add_argument('--output', action='store', help='Output file name', metavar='<OUTPUT>')
    parser.add_argument('--bruteforce', action='store_true', default=False, help='Download without waiting')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0.0')

    args = vars(parser.parse_args())
    get_map(args)
