#!/usr/bin/python
import os
import requests
import urllib2
import profile
from StringIO import StringIO
from PIL import Image

headers = {
        'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'en-US,en;q=0.5',
        'Connection': 'keep-alive',
        'Host': 'wxs.ign.fr',
        'Referer': 'http://www.geoportail.gouv.fr/swf/geoportal-visu-1.3.2.swf',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0',
}

def testRequest():

    url_prefix = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&'
    url_appendix = '&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='

    row_start = 11320
    col_start = 16620
    col_span = row_span = 5
    ### Create an empty map
    my_map = Image.new('RGB', (256 * col_span, 256 * row_span))

    for row in xrange(row_span):
        for col in xrange(col_span):

            image_name = str(row_start + row) + '-' + str(col_start + col) + '.jpg'
            url = url_prefix + 'TILEMATRIX=15&TILEROW=' + str(row_start + row) + '&TILECOL=' + str(col_start + col) + url_appendix

            r = requests.get(url, headers=headers)
            im = Image.open(StringIO(r.content))

            try:
                my_map.paste(im, (col * 256, row * 256))
            except IOError, e:
                pass

    my_map.save('my_map_with_request.jpg')


if __name__ == '__main__':
    profile.run('testRequest()')
