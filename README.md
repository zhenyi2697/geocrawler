## Description

This is the script for dwonloading map from http://www.geoportail.fr

## Installation

This is script requires python PIL (with jpeg decoder enabled) and requests package. To setup the environment, follow the instructions below:

### Ubuntu

1. Install pip

``` 
$ sudo apt-get install pip

```
	
2. Install requests

``` 
$ sudo pip install requests
```

3. Install PIL

	The script requires the jpeg decoder enabled. If you have already installed PIL using pip, please uninstall it first.

```
If you have already installed PIL without jpeglib, unistall first
$ sudo pip uninstall PIL
	
Reinstall PIL (For x64 OS)
$ sudo apt-get install libjpeg8-dev
$ sudo ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /usr/lib
$ sudo pip install PIL

Reinstall PIL (For x32 OS)
$ sudo apt-get install libjpeg8-dev
$ sudo ln -s /usr/lib/i386-linux-gnu/libjpeg.so /usr/lib
$ sudo pip install PIL
```

TI confirmed this works for Ubuntu 12.04 LTS

### Mac OS (Mountain Lion)

In Mac OS, we follow almost the same instructions, except that PIL requires a little more work.

1. Install pip

```
$ sudo brew install pip
```

2. Install requests

```
$ sudo pip install requests
```
3. Install PIL

	Notice that this is the only working way that I have found
	
```
Download jpeg lib here: http://www.ijg.org/files/jpegsrc.v7.tar.gz

$ tar -zxvf jpegsrc.v7.tar.gz
$ cd jpeg-7

$ ./configure
$ sudo make clean
$ sudo CC="gcc -arch i386"./configure --enable-shared --enable-static
$ sudo make
$ sudo make install

Download Imaging package here: http://effbot.org/downloads/Imaging-1.1.6.tar.gz

$ tar -zxvf Imaging-1.1.6.tar.gz
$ cd Imaging-1.1.6
$ sudo rm -Rf build

In setup.py, edit like this:
JPEG_ROOT = libinclude("/usr/local/lib")

$ sudo python setup.py build
$ sudo python setup.py install
```
	
Now, we have the environment setted up!

### Windows

What? You use Windows? Well, you can do some washing and go to bed …

## Usage

This script is a command-line tool for downloading map from geoportail.fr
Since the database is very large, you must specify which region you want to download. If you type `python get_map.py --help`, you will see the following output:

```
usage: get_map.py [-h] --row <ROW_NUM> --column <COL_NUM> --span <SPAN>
                  [--matrix <MATRIX>] [--output <OUTPUT>] [--version]

Script for getting map from geoportail.fr

optional arguments:
  -h, --help          show this help message and exit
  --row <ROW_NUM>     Row of start point
  --column <COL_NUM>  Column of start point
  --span <SPAN>       Size of map (number of tile map)
  --matrix <MATRIX>   The tile matrix number
  --output <OUTPUT>   Output file name
  --version           show program's version number and exit
```

Well, you must find the row and column of the region you want to download. Then, how to?  

The answer is: I'm not going to teach you hacking the site. Find it yourself!!

## License

This code is distributed under the terms and conditions of the [MIT license](http://opensource.org/licenses/MIT).

## Copyright and legal issue

This script is only for academic use.   
Do not use this script and all its related results for business purpose.   
The author is not responsable for any abuse of this program

@ copyright zhenyi2697 at gmail.com
