#!/usr/bin/python
import requests
import urllib
import urllib2
import profile

def testUrllib():
    image_name = 'test1.jpg'
    url = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX=15&TILEROW=11320&TILECOL=16620&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='
    urllib.urlretrieve(url, image_name)

def testUrllib2():
    image_name = 'test2.jpg'
    url = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX=15&TILEROW=11320&TILECOL=16620&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='
    r = urllib2.urlopen(url)
    with open(image_name, 'w+') as f:
        f.write(r.read())

def testRequest():
    image_name = 'test3.jpg'
    url = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX=15&TILEROW=11320&TILECOL=16620&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='

    r = requests.get(url)
    # with open(image_name, 'wb') as f:
        # for chunk in r.iter_content():
            # f.write(chunk)
    from StringIO import StringIO
    from PIL import Image
    i = Image.open(StringIO(r.content))
    i.save(image_name)

def testRequest2():
    image_name = 'test4.jpg'
    url = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX=15&TILEROW=11320&TILECOL=16620&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='

    r = requests.get(url, stream=True)
    with open(image_name, 'wb') as f:
        for chunk in r.iter_content():
            f.write(chunk)

if __name__ == '__main__':
    profile.run('testUrllib()')
    profile.run('testUrllib2()')
    profile.run('testRequest()')
    profile.run('testRequest2()')
